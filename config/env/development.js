'use strict';

module.exports = {
  db: 'mongodb://bing:83387012@ds053429.mongolab.com:53429/zhibinma',
  debug: true,
  logging: {
    format: 'tiny'
  },
  //  aggregate: 'whatever that is not false, because boolean false value turns aggregation off', //false
  aggregate: false,
  mongoose: {
    debug: false
  },
  hostname: 'http://localhost:3000',
  app: {
    name: 'MEAN - A Modern Stack - Development'
  },
  strategies: {
    local: {
      enabled: true
    },
    landingPage: '/',
    facebook: {
      clientID: '547829778566665',
      clientSecret: '70368b178e4cc4eff8eab16d9b4a1096',
      callbackURL: 'http://zhibin.ma/api/auth/facebook/callback',
      enabled: true
    },
    twitter: {
      clientID: 'CONSUMER_KEY',
      clientSecret: 'CONSUMER_SECRET',
      callbackURL: 'http://localhost:3000/api/auth/twitter/callback',
      enabled: false
    },
    github: {
      clientID: 'APP_ID',
      clientSecret: 'APP_SECRET',
      callbackURL: 'http://localhost:3000/api/auth/github/callback',
      enabled: false
    },
    google: {
      clientID: 'APP_ID',
      clientSecret: 'APP_SECRET',
      callbackURL: 'http://localhost:3000/api/auth/google/callback',
      enabled: false
    },
    linkedin: {
      clientID: 'API_KEY',
      clientSecret: 'SECRET_KEY',
      callbackURL: 'http://localhost:3000/api/auth/linkedin/callback',
      enabled: false
    },
    //Bing Ma
    bnet: {
      clientID: 'm5e9q5ubwfcbpdrrrbcmdfhgcvmahbe3',
      clientSecret: 'gsbaAg7Q9KSDspvVAHtH8e3vwEp359Ev',
      callbackURL: 'http://zhibin.ma/api/auth/bnet/callback',
      enabled: true
    }
  },
  emailFrom: 'zhibin.ma@live.com', // sender address like ABC <abc@example.com>
  mailer: {
    host: 'email-smtp.us-west-2.amazonaws.com',
    port: 465,
    secure: true,
    auth: {
      user: 'AKIAJ6BGELJUK5E3DMMQ',
      pass: 'AkElMoWnrqX1rEr3xpvOJul4Eu/YW17dpBiOUxaaxAN5'
    }
  },
  secret: 'SOME_TOKEN_SECRET'
};
