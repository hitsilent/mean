'use strict';

module.exports = {
  db: 'mongodb://' + (process.env.DB_PORT_27017_TCP_ADDR) + '/zhibinma',
  /**
   * Database options that will be passed directly to mongoose.connect
   * Below are some examples.
   * See http://mongodb.github.io/node-mongodb-native/driver-articles/mongoclient.html#mongoclient-connect-options
   * and http://mongoosejs.com/docs/connections.html for more information
   */
  dbOptions: {
    /*
    server: {
        socketOptions: {
            keepAlive: 1
        },
        poolSize: 5
    },
    replset: {
      rs_name: 'myReplicaSet',
      poolSize: 5
    },
    db: {
      w: 1,
      numberOfRetries: 2
    }
    */
  },
  hostname: 'https://zhibin-ma.herokuapp.com/',
  app: {
    name: 'Bing Ma - Welcome to MEAN, a fullstack javascript framework'
  },
  logging: {
    //format: 'combined'
    format: 'tiny'
  },
  strategies: {
    local: {
      enabled: true
    },
    landingPage: '/',
    facebook: {
      clientID: '547829778566665',
      clientSecret: '70368b178e4cc4eff8eab16d9b4a1096',
      callbackURL: 'http://zhibin.ma/api/auth/facebook/callback',
      enabled: true
    },
    twitter: {
      clientID: 'CONSUMER_KEY',
      clientSecret: 'CONSUMER_SECRET',
      callbackURL: 'http://localhost:3000/api/auth/twitter/callback',
      enabled: false
    },
    github: {
      clientID: 'APP_ID',
      clientSecret: 'APP_SECRET',
      callbackURL: 'http://localhost:3000/api/auth/github/callback',
      enabled: false
    },
    google: {
      clientID: 'APP_ID',
      clientSecret: 'APP_SECRET',
      callbackURL: 'http://localhost:3000/api/auth/google/callback',
      enabled: false
    },
    linkedin: {
      clientID: 'API_KEY',
      clientSecret: 'SECRET_KEY',
      callbackURL: 'http://localhost:3000/api/auth/linkedin/callback',
      enabled: false
    },
    //Bing Ma
    bnet: {
      clientID: 'm5e9q5ubwfcbpdrrrbcmdfhgcvmahbe3',
      clientSecret: 'gsbaAg7Q9KSDspvVAHtH8e3vwEp359Ev',
      callbackURL: 'http://zhibin.ma/api/auth/bnet/callback',
      enabled: true
    }
  },
  emailFrom: 'zhibin.ma@live.com', // sender address like ABC <abc@example.com>
  mailer: {
    host: 'email-smtp.us-west-2.amazonaws.com',
    port: 465,
    secure: true,
    auth: {
      user: 'AKIAJ6BGELJUK5E3DMMQ',
      pass: 'AkElMoWnrqX1rEr3xpvOJul4Eu/YW17dpBiOUxaaxAN5'
    }
  },
  secret: 'SOME_TOKEN_SECRET'
};
